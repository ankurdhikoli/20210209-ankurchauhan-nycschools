# 20210209-AnkurChauhan-NYCSchools


 Created the UI programmatically no Storyboard and XIB used.
 **SchoolListingViewController** - Used TableViewController
**SchoolSatScoreViewcontroller** - Used autolayout constraints to create UI
 Added logo on navigation bar.

# Objective C Use

Created **LoadingViewController** in Objective-C which presents before data comes from the screen. for showing knowledge of language and  bridging header to use

# Architecture

## Wrote decoupled code for view controllers with the help of dependency injections and protocols which helps in testing.
## Added MVVM for School listing view controller.
## Added MVC approach for SatScore Viewcontroller


# Unit Testing

1. Testing for webservices by mocking Webservice class.
2. Testing for viewmodel
3. Verifing the Viewcontroller UI with the help of Unit Testing 

# UI Testing

## Verified School Listing and Sat Score screen UI


# Routing - Coordinators (ToDo)
Rounting we can use coordintor but for now i have used simple push code in ViewController because of less time and 2 screens only.
