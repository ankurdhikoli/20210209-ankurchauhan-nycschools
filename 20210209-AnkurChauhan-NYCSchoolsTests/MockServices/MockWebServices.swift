//
//  MockWebServices.swift
//  20210209-AnkurChauhan-NYCSchoolsTests
//
//  Created by Ankur on 2/14/21.
//

import Foundation
@testable import _0210209_AnkurChauhan_NYCSchools

final class MockWebService {
    var hasResponse = false
}

extension MockWebService: WebAPIServiceProtocol {
    
    func getSchoolInfomation(schoolInformationCompletionHandler: @escaping (Result<[SchoolData]?, NetworkError>) -> Void) {
        if hasResponse {
            let mockResponse = MockData.getMockSchoolData()
            schoolInformationCompletionHandler(.success(mockResponse))
        } else {
            schoolInformationCompletionHandler(.failure(.noData))
        }
    }
    
    func getSATScores(for schoolID: String, SATscoreCompletionHandler: @escaping (Result<[ScoresData]?, NetworkError>) -> Void) {
        if hasResponse {
            let mockResponse = MockData.getMockSatScoreData()
            SATscoreCompletionHandler(.success(mockResponse))
        } else {
            SATscoreCompletionHandler(.failure(.noData))
        }
    }
    
    
}
