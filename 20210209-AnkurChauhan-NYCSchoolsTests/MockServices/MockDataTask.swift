//
//  MockDataTask.swift
//  20210209-AnkurChauhan-NYCSchoolsTests
//
//  Created by Ankur on 2/14/21.
//

import Foundation

class MockDataTask: URLSessionDataTask {
    private let data: Data?
    private let urlResponse: URLResponse?
    internal let taskError : Error?
    var completionHandler: ((Data?, URLResponse?, Error?) -> Void)!

    init(data: Data?, urlResponse: URLResponse?, error: Error?) {
        self.data = data
        self.urlResponse = urlResponse
        self.taskError = error
    }
    
    override func resume() {
        DispatchQueue.main.async {
            self.completionHandler(self.data, self.urlResponse, self.taskError)
        }
    }
}
