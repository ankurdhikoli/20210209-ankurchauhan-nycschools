//
//  MockURLSession.swift
//  20210209-AnkurChauhan-NYCSchoolsTests
//
//  Created by Ankur on 2/14/21.
//

import Foundation

class MockUrlSession: URLSession {
    var cachedUrl: URL?
    let mockTask: MockDataTask
    
    init(data: Data?, urlResponse: URLResponse?, error: Error?) {
        mockTask = MockDataTask(data: data, urlResponse: urlResponse, error: error)
    }
    override func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        mockTask.completionHandler = completionHandler
        return mockTask
    }
}
