//
//  MockData.swift
//  20210209-AnkurChauhan-NYCSchoolsTests
//
//  Created by Ankur on 2/14/21.
//

import Foundation
@testable import _0210209_AnkurChauhan_NYCSchools

class MockData {
    
    static let schoolData = """
[{
    "dbn": "53",
    "school_name": "SampleSchool"
}]
"""
    static let schoolScores = """
[{
    "sat_math_avg_score": "100",
    "sat_critical_reading_avg_score": "200",
    "sat_writing_avg_score": "300"
}]
"""
    
    static func getMockSchoolData() -> [SchoolData]? {
        return try! JSONDecoder().decode([SchoolData].self, from: MockData.schoolData.data(using: .utf8)!)
    }
    
    static func getMockSatScoreData() -> [ScoresData]? {
        return try! JSONDecoder().decode([ScoresData].self, from: MockData.schoolScores.data(using: .utf8)!)
    }
    
}




