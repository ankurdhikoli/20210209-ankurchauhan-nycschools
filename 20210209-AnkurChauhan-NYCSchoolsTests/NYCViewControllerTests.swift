//
//  NYCViewControllerTests.swift
//  20210209-AnkurChauhan-NYCSchoolsTests
//
//  Created by Ankur on 2/14/21.
//

import XCTest
@testable import _0210209_AnkurChauhan_NYCSchools

class NYCViewControllerTests: XCTestCase {

    var appDelegate: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    override func setUp() {
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        appDelegate.window!.makeKeyAndVisible()
    }
    
    override func tearDown() {
        appDelegate.window = nil
      }

    
    func testSchoolListingControllerUI() {
        let serviceMock = MockWebService()
        let model = SchoolListingViewModel(serviceMock)
        serviceMock.hasResponse = true
        let vc = SchoolListingViewController(model)
        appDelegate.window!.rootViewController = vc

        vc.loadViewIfNeeded()
        
        vc.listingViewModel.onSuccess = {
            let tableView = vc.view as! UITableView
            if tableView.visibleCells.isEmpty {
                XCTFail()
            }
        }
            vc.listingViewModel.getSchools()
    }
}
