//
//  WebServiceTests.swift
//  20210209-AnkurChauhan-NYCSchoolsTests
//
//  Created by Ankur on 2/14/21.
//

import XCTest
@testable import _0210209_AnkurChauhan_NYCSchools

class WebServiceTests: XCTestCase {
    
    func testSchoolListing_ReturnsFailResponse() throws {

        var errorResponse: NetworkError?

        let errorResponseExpectation = expectation(description: "errorResponseExpectation")
        let jsonData = "{}".data(using: .utf8)
        let response = HTTPURLResponse(url: URL(string: "https://www.somefakeurl.com")!, statusCode: 200,
                                       httpVersion: nil, headerFields: nil)!
        let mockURLSession  = MockUrlSession(data: jsonData, urlResponse: response, error: nil)
        let schoolService = WebService(session: mockURLSession)

        
        schoolService.getSchoolInfomation { result in
            switch result {
            case .success:
                    XCTFail()
            case .failure(let error):
                errorResponse = error
                errorResponseExpectation.fulfill()
            }
        }
        waitForExpectations(timeout: 1) { error in
            XCTAssertNotNil(errorResponse)
        }
        
    }

    func testSchoolListing_ReturnsSuccessResponse() throws {

        var schoolInfo: [SchoolData]?
        let schoolInfoExpectation = expectation(description: "SchoolInfo")
        let jsonData = MockData.schoolData.data(using: .utf8)
        let response = HTTPURLResponse(url: URL(string: "https://www.somefakeurl.com")!, statusCode: 200,
                                       httpVersion: nil, headerFields: nil)!
        let mockURLSession  = MockUrlSession(data: jsonData, urlResponse: response, error: nil)
        let schoolService = WebService(session: mockURLSession)

        
        schoolService.getSchoolInfomation { result in
            switch result {
            case .success(let response):
                if let info = response {
                    schoolInfo = info
                    schoolInfoExpectation.fulfill()
                }
            case .failure(let error):
                print(error)
                XCTFail()
            }
        }
        waitForExpectations(timeout: 1) { error in
            XCTAssertNotNil(schoolInfo)
        }
    }
    
    func testSatScores_ReturnsFailResponse() throws {
      
//        service.getSATScores(for: "FakeID") { result in
//
//        }
    }

    func testSatScores_ReturnSuccessResponse() throws {
       
        var schoolSatInfo: [ScoresData]?
        let schoolInfoExpectation = expectation(description: "SatScoresData")
        //service.hasResponse = true

        let jsonData = MockData.schoolScores.data(using: .utf8)
        let response = HTTPURLResponse(url: URL(string: "https://www.somefakeurl.com")!, statusCode: 200, httpVersion: nil, headerFields: nil)!
        let mockURLSession  = MockUrlSession(data: jsonData, urlResponse: response, error: nil)
        let schoolService = WebService(session: mockURLSession)
        schoolService.getSATScores(for: "FakeID") { result in
            
            switch result {
            case .success(let response):
                if let info = response {
                    schoolSatInfo = info
                    schoolInfoExpectation.fulfill()
                }
            case .failure:
                XCTFail()
            }
        }
        waitForExpectations(timeout: 1) { error in
            XCTAssertNotNil(schoolSatInfo)
        }
    }

}
