//
//  SchoolListingViewModelTests.swift
//  20210209-AnkurChauhan-NYCSchoolsTests
//
//  Created by Ankur on 2/14/21.
//

import XCTest

@testable import _0210209_AnkurChauhan_NYCSchools

class SchoolListingViewModelTests: XCTestCase {

 
    var sut: SchoolListingViewModel!
    var serviceMock = MockWebService()

    func testGetSchools_ReturnResponse() {
        sut = SchoolListingViewModel(serviceMock)
        serviceMock.hasResponse = true
        sut.onSuccess = {
            XCTAssertTrue(self.sut.schoolInformation.count != 0)
        }
        sut.getSchools()
    }

    func testGetSchools_ReturnFailResponse()  {
        sut = SchoolListingViewModel(serviceMock)
        sut.onFailure = {
            XCTAssertNotNil($0)
        }
        sut.getSchools()
    }

}
