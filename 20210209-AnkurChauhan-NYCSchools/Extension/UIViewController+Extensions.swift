//
//  UIViewController+Extensions.swift
//  20210209-AnkurChauhan-NYCSchools
//
//  Created by Ankur on 2/13/21.
//

extension UIViewController {
    func setNavigationBarLogo() {
        let logo = UIImage(named: "NYCLogo")
         let imageView = UIImageView(image: logo)
        imageView.contentMode = .scaleAspectFit
         self.navigationItem.titleView = imageView
    }
}
