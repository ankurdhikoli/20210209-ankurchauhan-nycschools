//
//  WebService.swift
//  20210209-AnkurChauhan-NYCSchools
//
//  Created by Ankur on 2/9/21.
//

import Foundation

enum NetworkError: Swift.Error {
    case noData
    case invalidEndPoints
    case parseError(String)
    case unknown(String)
}

extension NetworkError {
    var description: String {
        switch self {
        case .noData:
            return "Error no data."
        case .invalidEndPoints:
            return "Request timeout"
        case .parseError(let error):
            return "Not able parse the data: \(error)"
        case .unknown(let error):
            return "An unknown error has occured: \(error)"
        }
    }
}

protocol WebAPIServiceProtocol {
    func getSchoolInfomation(schoolInformationCompletionHandler: @escaping (Result<[SchoolData]?, NetworkError>) -> Void)
    func getSATScores(for schoolID: String, SATscoreCompletionHandler: @escaping (Result<[ScoresData]?, NetworkError>) -> Void)
}


final class WebService {
    // MARK:- Variables And Properties
    private let session: URLSession
    private var dataTask: URLSessionDataTask?
    init(session: URLSession) {
        self.session = session
    }
    
}

extension WebService: WebAPIServiceProtocol {
    
    
    /// - Parameter schoolInformationCompletionHandler: school metadata to be used in our program after the HTTP response is received
    public func getSchoolInfomation(schoolInformationCompletionHandler: @escaping (Result<[SchoolData]?, NetworkError>) -> Void) {
        
        guard let url = URL(string: Endpoints.schoolDirectory.rawValue) else {
            print("Failed to make school directory endpoint URL")
            return
        }
        
        load(url: url, completion: schoolInformationCompletionHandler)
    }
    
    
    /// Get the SAT scores (math, reading, writing) for a school that was selected from the table view
    ///
    /// - Parameter schoolID: unique school identifier from the dbn field in the API
    public func getSATScores(for schoolID: String, SATscoreCompletionHandler: @escaping (Result<[ScoresData]?, NetworkError>)->()) {
        
        //Create search query for specific dbn (school ID)
        var urlComponents = URLComponents(string: Endpoints.SATScores.rawValue)
        urlComponents?.query = "dbn=\(schoolID)"
        
        guard let url = urlComponents?.url else {
            print("Couldn't create the get SATs URL.")
            return
        }
        load(url: url, completion: SATscoreCompletionHandler)
    }
    
    
    func load<T: Decodable>(url: URL, completion: @escaping (Result<[T]?, NetworkError>)->()) {
        dataTask =  session.dataTask(with: url) { data, response, error in
            
            if let error = error {
                completion(.failure(.unknown(error.localizedDescription)))
            } else if let data = data {
                let response = response as? HTTPURLResponse
                do {
                    let decoder = JSONDecoder()
                    let decodedData = try decoder.decode([T].self, from: data)
                    if response?.statusCode == 200 {
                        DispatchQueue.main.async {
                            completion(.success(decodedData))
                        }
                    } else {
                        completion(.failure(.noData))
                    }
                    
                } catch (let error) {
                    completion(.failure(.parseError(error.localizedDescription)))
                    return
                }
            }
        }
        dataTask?.resume()
    }
    
}
