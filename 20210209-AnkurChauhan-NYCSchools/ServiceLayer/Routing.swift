//
//  Routing.swift
//  20210209-AnkurChauhan-NYCSchools
//
//  Created by Ankur on 2/9/21.
//

import Foundation

enum Endpoints: String {
    case schoolDirectory = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    case SATScores = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
}
