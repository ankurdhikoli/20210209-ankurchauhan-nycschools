//
//  SchoolModel.swift
//  20210209-AnkurChauhan-NYCSchools
//
//  Created by Ankur on 2/9/21.
//

import Foundation

struct SchoolData: Decodable {
    let schoolUniqueID: String
    let schoolName: String
    
    private enum CodingKeys: String, CodingKey {
        case schoolUniqueID = "dbn"
        case schoolName = "school_name"
    }
}
