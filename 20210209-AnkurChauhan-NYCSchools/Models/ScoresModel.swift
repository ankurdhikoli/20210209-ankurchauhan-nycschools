//
//  ScoresModel.swift
//  20210209-AnkurChauhan-NYCSchools
//
//  Created by Ankur on 2/9/21.
//

import Foundation

struct ScoresData: Decodable {
    let mathAverageScore: String
    let readingAverageScore: String
    let writingAverageScore: String
    
    private enum CodingKeys: String, CodingKey {
        case mathAverageScore = "sat_math_avg_score"
        case readingAverageScore = "sat_critical_reading_avg_score"
        case writingAverageScore = "sat_writing_avg_score"
    }
}

