//
//  SchoolListingViewModel.swift
//  20210209-AnkurChauhan-NYCSchools
//
//  Created by Ankur on 2/12/21.
//

import Foundation

protocol SchoolListingViewModelProtocol {
    var schoolInformation: [SchoolData] { get set }
    var services: WebAPIServiceProtocol { get set }
    var onSuccess: ()->() { get set }
    var onFailure: (NetworkError)->Void  { get set }
    var isServiceLoadRequired: Bool { get set }
    func getSchools()
}

class SchoolListingViewModel: SchoolListingViewModelProtocol  {
    
    internal var schoolInformation: [SchoolData] = [] {
        didSet {
            onSuccess()
        }
    }
    
    var services: WebAPIServiceProtocol
    var onSuccess: ()->() = { }
    var onFailure: (NetworkError)->Void = { _ in }
    var isServiceLoadRequired: Bool = true
    
    init(_ service: WebAPIServiceProtocol = WebService(session: URLSession(configuration: .default))) {
        self.services = service
    }
    
    func getSchools() {
        services.getSchoolInfomation { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let response):
                self.schoolInformation = response!
                self.isServiceLoadRequired = false
            case .failure(let error):
                self.onFailure(error)
            }
        }
        
    }
}
