//
//  LoadingViewController.m
//  20210209-AnkurChauhan-NYCSchools
//
//  Created by Ankur on 2/9/21.
//

#import "LoadingViewController.h"

@interface LoadingViewController ()
@property (nonatomic, strong) UIActivityIndicatorView * spinner;
@end


@implementation LoadingViewController


- (void)loadView {
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleLarge];
    self.spinner.translatesAutoresizingMaskIntoConstraints = false;
    [self.spinner startAnimating];

    self.view = [[UIView alloc] init];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.spinner];
    
    [NSLayoutConstraint activateConstraints:@[
        [self.spinner.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor],
        [self.spinner.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor]
    ]];

}

-(void) viewDidLoad {
    [super viewDidLoad];
}

-(void) startSpinnerFor :(UIViewController *) viewController {
    [self.spinner startAnimating];
    [viewController addChildViewController:self];
    self.view.frame = viewController.view.frame;
    [viewController.view addSubview:self.view];
    [self didMoveToParentViewController:viewController];
}

-(void) stopSpinner {
    [self.spinner stopAnimating];
    [self.view willMoveToSuperview:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

@end
