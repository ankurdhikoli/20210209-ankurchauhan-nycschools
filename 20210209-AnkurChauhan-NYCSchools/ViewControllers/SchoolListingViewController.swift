//
//  SchoolsViewController.swift
//  20210209-AnkurChauhan-NYCSchools
//
//  Created by Ankur on 2/9/21.
//

import UIKit

class SchoolListingViewController: UITableViewController {

    private let activityView = LoadingViewController()
    var listingViewModel: SchoolListingViewModelProtocol
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if listingViewModel.isServiceLoadRequired {
            activityView.startSpinner(for: self)
        }
    }
    
    init(_ model: SchoolListingViewModel = SchoolListingViewModel()) {
        self.listingViewModel = model
        super.init(style: .plain)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        
        listingViewModel.onSuccess = { [weak self] in
            guard let self = self else { return }
            self.tableView.reloadData()
            self.activityView.stopSpinner()
        }

        listingViewModel.onFailure = { error in
            print(error.description)
            self.activityView.stopSpinner()
        }
        
        listingViewModel.getSchools()
        setupView()
    }
    
   
    func setupView() {
        self.setNavigationBarLogo()
        tableView.separatorColor = .black
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listingViewModel.schoolInformation.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let schoolInfo = listingViewModel.schoolInformation[indexPath.row]
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = schoolInfo.schoolName
        return cell
    }
    
    // MARK: - Table view Delegate

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let uniqueId = listingViewModel.schoolInformation[indexPath.row].schoolUniqueID
        let detialVC = SatScoreViewController(uniqueId)
        //self.present(detialVC, animated: true, completion: nil)
        self.navigationController?.pushViewController(detialVC, animated: true)
    }

}
