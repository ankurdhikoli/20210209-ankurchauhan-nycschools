//
//  ViewController.swift
//  20210209-AnkurChauhan-NYCSchools
//
//  Created by Ankur on 2/9/21.
//

import UIKit

class SatScoreViewController: UIViewController {
    
    private let webService : WebAPIServiceProtocol
    private let uniqueId : String
    let activityView = LoadingViewController()
    
    init(_ id: String, webService: WebAPIServiceProtocol = WebService(session: URLSession(configuration: .default))) {
        self.uniqueId = id
        self.webService = webService
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        getSatData()
        activityView.startSpinner(for: self)
    }
    
    func createUIWithData(_ scoreData: ScoresData) {
        let mathAverageScore = UILabel()
        self.view.addSubview(mathAverageScore)
        mathAverageScore.translatesAutoresizingMaskIntoConstraints = false
        mathAverageScore.text = "Math Score: \(scoreData.mathAverageScore)"
        mathAverageScore.textAlignment = .center
        
        let readingAverageScore = UILabel()
        self.view.addSubview(readingAverageScore)
        readingAverageScore.translatesAutoresizingMaskIntoConstraints = false
        readingAverageScore.text = "Reading Score: \(scoreData.readingAverageScore)"
        readingAverageScore.textAlignment = .center
        
        let writingAverageScore = UILabel()
        self.view.addSubview(writingAverageScore)
        writingAverageScore.translatesAutoresizingMaskIntoConstraints = false
        writingAverageScore.text = "Writing Score: \(scoreData.writingAverageScore)"
        writingAverageScore.textAlignment = .center
        
        NSLayoutConstraint.activate(
            [
                mathAverageScore.topAnchor.constraint(equalTo: self.view.layoutMarginsGuide.topAnchor, constant: 100),
                mathAverageScore.leadingAnchor.constraint(equalTo: self.view.layoutMarginsGuide.leadingAnchor),
                mathAverageScore.trailingAnchor.constraint(equalTo: self.view.layoutMarginsGuide.trailingAnchor),
                mathAverageScore.heightAnchor.constraint(equalToConstant: 50),
                
                readingAverageScore.topAnchor.constraint(equalTo: mathAverageScore.bottomAnchor),
                readingAverageScore.leadingAnchor.constraint(equalTo: self.view.layoutMarginsGuide.leadingAnchor),
                readingAverageScore.trailingAnchor.constraint(equalTo: self.view.layoutMarginsGuide.trailingAnchor),
                readingAverageScore.heightAnchor.constraint(equalToConstant: 50),
                
                
                writingAverageScore.topAnchor.constraint(equalTo: readingAverageScore.bottomAnchor),
                writingAverageScore.leadingAnchor.constraint(equalTo: self.view.layoutMarginsGuide.leadingAnchor),
                writingAverageScore.trailingAnchor.constraint(equalTo: self.view.layoutMarginsGuide.trailingAnchor),
                writingAverageScore.heightAnchor.constraint(equalToConstant: 50)
            ]
        )
    }
    
    func getSatData() {
        webService.getSATScores(for: uniqueId) { [weak self] result in
            self?.activityView.stopSpinner()
            switch result {
            case .success(let response):
                if let score = response?.last {
                    self?.createUIWithData(score)
                } else {
                    self?.createNoDataAlert()
                }
            case .failure(let eror):
                print(eror.description)
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createNoDataAlert() {
        let alertController = UIAlertController(title: "Alert", message: "No Sat Score Available for School", preferredStyle: .alert)
        let actionOk = UIAlertAction(title: "Ok",
                                     style: .default,
                                     handler: { _ in
                                        self.navigationController?.popViewController(animated: true)
                                     })
        
        alertController.addAction(actionOk)
        self.present(alertController, animated: true, completion: nil)
    }
}

