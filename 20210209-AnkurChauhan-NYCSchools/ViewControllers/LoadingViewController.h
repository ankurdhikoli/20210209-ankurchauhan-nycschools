//
//  LoadingViewController.h
//  20210209-AnkurChauhan-NYCSchools
//
//  Created by Ankur on 2/9/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoadingViewController : UIViewController

-(void) startSpinnerFor :(UIViewController *) viewController;
-(void) stopSpinner;

@end

NS_ASSUME_NONNULL_END
