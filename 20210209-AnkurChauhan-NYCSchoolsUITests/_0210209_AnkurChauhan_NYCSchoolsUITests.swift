//
//  _0210209_AnkurChauhan_NYCSchoolsUITests.swift
//  20210209-AnkurChauhan-NYCSchoolsUITests
//
//  Created by Ankur on 2/9/21.
//

import XCTest


class _0210209_AnkurChauhan_NYCSchoolsUITests: XCTestCase {
    
    var app: XCUIApplication!
    
    override func setUp() {
        app = XCUIApplication()
        app.launch()
        continueAfterFailure = false
    }

    
    func testSchoolInfoScreen() {
        let table =  app.tables.element
        XCTAssert(table.exists, "School Info Table does not exist")
        
        let cells = table.cells.element(boundBy: 1)
        cells.tap()
        
        let promise = expectation(description: "wait for sat score screen to present")
        let timeout: TimeInterval = 2
        DispatchQueue.main.asyncAfter(deadline: .now() + timeout) {
            promise.fulfill()
        }
        wait(for: [promise], timeout: timeout)
        
        let readLabel = app.staticTexts.element(boundBy: 0)
        let writingLabel = app.staticTexts.element(boundBy: 1)
        let mathLabel = app.staticTexts.element(boundBy: 2)
        
        XCTAssert(mathLabel.exists, "mathLabel does not exist")
        XCTAssert(readLabel.exists, "readLabel does not exist")
        XCTAssert(writingLabel.exists, "writingLabel does not exist")
    }
    
    func testSchoolSatScoreScreen() {
        
        let table =  app.tables.element
        XCTAssert(table.exists, "School Info Table does not exist")
        
        let cells = table.cells.element(boundBy: 1)
        cells.tap()
                
        let promise = expectation(description: "wait for sat score screen to present")
        let timeout: TimeInterval = 2
        DispatchQueue.main.asyncAfter(deadline: .now() + timeout) {
            promise.fulfill()
        }
        wait(for: [promise], timeout: timeout)
        
        let mathLabel = app.staticTexts.element(boundBy: 0)
        let readLabel = app.staticTexts.element(boundBy: 1)
        let writingLabel = app.staticTexts.element(boundBy: 2)
        
        XCTAssert(mathLabel.exists, "mathLabel does not exist")
        XCTAssert(readLabel.exists, "readLabel does not exist")
        XCTAssert(writingLabel.exists, "writingLabel does not exist")

    }
    
    override func tearDown() {
        app = nil
    }
}
